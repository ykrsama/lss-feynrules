# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.3.1 for Mac OS X ARM (64-bit) (July 24, 2021)
# Date: Thu 20 Jul 2023 19:54:05


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(aah1*cTheta*complex(0,1))',
                order = {'HIW':1})

GC_2 = Coupling(name = 'GC_2',
                value = '-0.3333333333333333*(ee*complex(0,1))',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '(ee**2*complex(0,1))/(2.*cw)',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-0.5*(cTheta*ee**2)/cw',
                order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '(cTheta*ee**2)/(2.*cw)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-G',
                 order = {'QCD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '-(cTheta*complex(0,1)*ggh1)',
                 order = {'HIG':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(cTheta*G*ggh1)',
                 order = {'HIG':1,'QCD':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'cTheta*complex(0,1)*G**2*ggh1',
                 order = {'HIG':1,'QCD':2})

GC_16 = Coupling(name = 'GC_16',
                 value = 'cw*complex(0,1)*gw',
                 order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '-(complex(0,1)*gw**2)',
                 order = {'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = 'cw**2*complex(0,1)*gw**2',
                 order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '-2*complex(0,1)*lmda',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '-4*complex(0,1)*lmda',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '-6*complex(0,1)*lmda',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '-(ee*MW)',
                 order = {'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = 'ee*MW',
                 order = {'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-0.5*(ee**2*sTheta)/cw',
                 order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = '(ee**2*sTheta)/(2.*cw)',
                 order = {'QED':2})

GC_26 = Coupling(name = 'GC_26',
                 value = 'complex(0,1)*ggh2*sTheta',
                 order = {'HIG':1})

GC_27 = Coupling(name = 'GC_27',
                 value = 'G*ggh2*sTheta',
                 order = {'HIG':1,'QCD':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '-(complex(0,1)*G**2*ggh2*sTheta)',
                 order = {'HIG':1,'QCD':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '-(a2*cTheta*complex(0,1)*sTheta) + 2*cTheta*complex(0,1)*lmda*sTheta',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = '-2*cTheta**2*complex(0,1)*lmda - a2*complex(0,1)*sTheta**2',
                 order = {'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '-(a2*cTheta**2*complex(0,1)) - 2*complex(0,1)*lmda*sTheta**2',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '-3*a2*cTheta**3*complex(0,1)*sTheta + 6*cTheta**3*complex(0,1)*lmda*sTheta + 3*a2*cTheta*complex(0,1)*sTheta**3 - 6*b4*cTheta*complex(0,1)*sTheta**3',
                 order = {'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '3*a2*cTheta**3*complex(0,1)*sTheta - 6*b4*cTheta**3*complex(0,1)*sTheta - 3*a2*cTheta*complex(0,1)*sTheta**3 + 6*cTheta*complex(0,1)*lmda*sTheta**3',
                 order = {'QED':2})

GC_34 = Coupling(name = 'GC_34',
                 value = '-(a2*cTheta**4*complex(0,1)) + 4*a2*cTheta**2*complex(0,1)*sTheta**2 - 6*b4*cTheta**2*complex(0,1)*sTheta**2 - 6*cTheta**2*complex(0,1)*lmda*sTheta**2 - a2*complex(0,1)*sTheta**4',
                 order = {'QED':2})

GC_35 = Coupling(name = 'GC_35',
                 value = '-6*cTheta**4*complex(0,1)*lmda - 6*a2*cTheta**2*complex(0,1)*sTheta**2 - 6*b4*complex(0,1)*sTheta**4',
                 order = {'QED':2})

GC_36 = Coupling(name = 'GC_36',
                 value = '-6*b4*cTheta**4*complex(0,1) - 6*a2*cTheta**2*complex(0,1)*sTheta**2 - 6*complex(0,1)*lmda*sTheta**4',
                 order = {'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '(cTheta**2*ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '-0.5*(cTheta*ee**2*complex(0,1)*sTheta)/sw**2',
                 order = {'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '(ee**2*complex(0,1)*sTheta**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '-0.5*(ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '(ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '(cTheta*ee)/(2.*sw)',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '-0.5*(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '-((cw*ee*complex(0,1))/sw)',
                 order = {'QED':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '-0.5*(ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = '-0.5*(cTheta*ee**2)/sw',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = '(cTheta*ee**2)/(2.*sw)',
                 order = {'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = '-0.5*(ee*MW)/sw',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(ee*MW)/(2.*sw)',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-0.5*(cTheta*ee*complex(0,1)*MW)/sw',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '-0.5*(ee*MZ)/sw',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '(ee*MZ)/(2.*sw)',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '-0.5*(cTheta*ee*complex(0,1)*MZ)/(cw*sw)',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '-0.5*(ee*sTheta)/sw',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '-0.5*(ee**2*sTheta)/sw',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '(ee**2*sTheta)/(2.*sw)',
                 order = {'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '(ee*complex(0,1)*MW*sTheta)/(2.*sw)',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '(ee*complex(0,1)*MZ*sTheta)/(2.*cw*sw)',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '-0.16666666666666666*(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = 'complex(0,1)*gw*sw',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-2*cw*complex(0,1)*gw**2*sw',
                 order = {'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = 'complex(0,1)*gw**2*sw**2',
                 order = {'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = '-0.5*(cw*ee*complex(0,1))/sw - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-0.5*(cw*ee*complex(0,1))/sw + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(cTheta*cw*ee)/(2.*sw) + (cTheta*ee*sw)/(2.*cw)',
                 order = {'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '(cw*ee**2*complex(0,1))/sw - (ee**2*complex(0,1)*sw)/cw',
                 order = {'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = '(cw*ee*MW)/(2.*sw) - (ee*MW*sw)/(2.*cw)',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '-0.5*(cw*ee*MW)/sw + (ee*MW*sw)/(2.*cw)',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '-0.5*(cw*ee*sTheta)/sw - (ee*sTheta*sw)/(2.*cw)',
                 order = {'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '-(ee**2*complex(0,1)) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = 'cTheta**2*ee**2*complex(0,1) + (cTheta**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cTheta**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_90 = Coupling(name = 'GC_90',
                 value = '-(cTheta*ee**2*complex(0,1)*sTheta) - (cTheta*cw**2*ee**2*complex(0,1)*sTheta)/(2.*sw**2) - (cTheta*ee**2*complex(0,1)*sTheta*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_91 = Coupling(name = 'GC_91',
                 value = 'ee**2*complex(0,1)*sTheta**2 + (cw**2*ee**2*complex(0,1)*sTheta**2)/(2.*sw**2) + (ee**2*complex(0,1)*sTheta**2*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_92 = Coupling(name = 'GC_92',
                 value = '-0.5*(ee**2*v)/cw',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(ee**2*v)/(2.*cw)',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '(cTheta*ee**2*complex(0,1)*v)/(2.*sw**2)',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '-0.5*(ee**2*complex(0,1)*sTheta*v)/sw**2',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '-0.5*(ee**2*v)/sw',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '(ee**2*v)/(2.*sw)',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '-0.5*(a1*complex(0,1)*sTheta) - 2*cTheta*complex(0,1)*lmda*v',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '-0.5*(a1*cTheta*complex(0,1)) + 2*complex(0,1)*lmda*sTheta*v',
                 order = {'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(-3*a1*cTheta**2*complex(0,1)*sTheta)/2. - 2*b3*complex(0,1)*sTheta**3 - 6*cTheta**3*complex(0,1)*lmda*v - 3*a2*cTheta*complex(0,1)*sTheta**2*v',
                  order = {'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = 'a1*cTheta**2*complex(0,1)*sTheta - 2*b3*cTheta**2*complex(0,1)*sTheta - (a1*complex(0,1)*sTheta**3)/2. - a2*cTheta**3*complex(0,1)*v + 2*a2*cTheta*complex(0,1)*sTheta**2*v - 6*cTheta*complex(0,1)*lmda*sTheta**2*v',
                  order = {'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '-0.5*(a1*cTheta**3*complex(0,1)) + a1*cTheta*complex(0,1)*sTheta**2 - 2*b3*cTheta*complex(0,1)*sTheta**2 - 2*a2*cTheta**2*complex(0,1)*sTheta*v + 6*cTheta**2*complex(0,1)*lmda*sTheta*v + a2*complex(0,1)*sTheta**3*v',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '-2*b3*cTheta**3*complex(0,1) - (3*a1*cTheta*complex(0,1)*sTheta**2)/2. + 3*a2*cTheta**2*complex(0,1)*sTheta*v + 6*complex(0,1)*lmda*sTheta**3*v',
                  order = {'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = 'cTheta*ee**2*complex(0,1)*v + (cTheta*cw**2*ee**2*complex(0,1)*v)/(2.*sw**2) + (cTheta*ee**2*complex(0,1)*sw**2*v)/(2.*cw**2)',
                  order = {'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '-(ee**2*complex(0,1)*sTheta*v) - (cw**2*ee**2*complex(0,1)*sTheta*v)/(2.*sw**2) - (ee**2*complex(0,1)*sTheta*sw**2*v)/(2.*cw**2)',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = 'yb/cmath.sqrt(2)',
                  order = {'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '-(CKM3x1*yb)',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '-(CKM3x2*yb)',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(CKM3x3*yb)',
                  order = {'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '-((cTheta*complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(complex(0,1)*sTheta*yb)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '-(yc/cmath.sqrt(2))',
                  order = {'QED':1})

GC_113 = Coupling(name = 'GC_113',
                  value = 'CKM1x2*yc',
                  order = {'QED':1})

GC_114 = Coupling(name = 'GC_114',
                  value = 'CKM2x2*yc',
                  order = {'QED':1})

GC_115 = Coupling(name = 'GC_115',
                  value = 'CKM3x2*yc',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-((cTheta*complex(0,1)*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '(complex(0,1)*sTheta*yc)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = 'yd/cmath.sqrt(2)',
                  order = {'QED':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '-(CKM1x1*yd)',
                  order = {'QED':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(CKM1x2*yd)',
                  order = {'QED':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(CKM1x3*yd)',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '-((cTheta*complex(0,1)*yd)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '(complex(0,1)*sTheta*yd)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '-ye',
                  order = {'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = 'ye',
                  order = {'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = 'ye/cmath.sqrt(2)',
                  order = {'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '-((cTheta*complex(0,1)*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(complex(0,1)*sTheta*ye)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '-ym',
                  order = {'QED':1})

GC_130 = Coupling(name = 'GC_130',
                  value = 'ym',
                  order = {'QED':1})

GC_131 = Coupling(name = 'GC_131',
                  value = 'ym/cmath.sqrt(2)',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-((cTheta*complex(0,1)*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '(complex(0,1)*sTheta*ym)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = 'ys/cmath.sqrt(2)',
                  order = {'QED':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '-(CKM2x1*ys)',
                  order = {'QED':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '-(CKM2x2*ys)',
                  order = {'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '-(CKM2x3*ys)',
                  order = {'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '-((cTheta*complex(0,1)*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '(complex(0,1)*sTheta*ys)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '-(yt/cmath.sqrt(2))',
                  order = {'QED':1})

GC_141 = Coupling(name = 'GC_141',
                  value = 'CKM1x3*yt',
                  order = {'QED':1})

GC_142 = Coupling(name = 'GC_142',
                  value = 'CKM2x3*yt',
                  order = {'QED':1})

GC_143 = Coupling(name = 'GC_143',
                  value = 'CKM3x3*yt',
                  order = {'QED':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-((cTheta*complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '(complex(0,1)*sTheta*yt)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '-ytau',
                  order = {'QED':1})

GC_147 = Coupling(name = 'GC_147',
                  value = 'ytau',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = 'ytau/cmath.sqrt(2)',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '-((cTheta*complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '(complex(0,1)*sTheta*ytau)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '-(yu/cmath.sqrt(2))',
                  order = {'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = 'CKM1x1*yu',
                  order = {'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = 'CKM2x1*yu',
                  order = {'QED':1})

GC_154 = Coupling(name = 'GC_154',
                  value = 'CKM3x1*yu',
                  order = {'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '-((cTheta*complex(0,1)*yu)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '(complex(0,1)*sTheta*yu)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = 'yd*complexconjugate(CKM1x1)',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '-(yu*complexconjugate(CKM1x1))',
                  order = {'QED':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '-(yc*complexconjugate(CKM1x2))',
                  order = {'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = 'yd*complexconjugate(CKM1x2)',
                  order = {'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = 'yd*complexconjugate(CKM1x3)',
                  order = {'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '-(yt*complexconjugate(CKM1x3))',
                  order = {'QED':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_167 = Coupling(name = 'GC_167',
                  value = 'ys*complexconjugate(CKM2x1)',
                  order = {'QED':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '-(yu*complexconjugate(CKM2x1))',
                  order = {'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '-(yc*complexconjugate(CKM2x2))',
                  order = {'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = 'ys*complexconjugate(CKM2x2)',
                  order = {'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = 'ys*complexconjugate(CKM2x3)',
                  order = {'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '-(yt*complexconjugate(CKM2x3))',
                  order = {'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = 'yb*complexconjugate(CKM3x1)',
                  order = {'QED':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '-(yu*complexconjugate(CKM3x1))',
                  order = {'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = 'yb*complexconjugate(CKM3x2)',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '-(yc*complexconjugate(CKM3x2))',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_182 = Coupling(name = 'GC_182',
                  value = 'yb*complexconjugate(CKM3x3)',
                  order = {'QED':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '-(yt*complexconjugate(CKM3x3))',
                  order = {'QED':1})

