# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.3.1 for Mac OS X ARM (64-bit) (July 24, 2021)
# Date: Thu 20 Jul 2023 19:54:05


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 2)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

HIG = CouplingOrder(name = 'HIG',
                    expansion_order = 99,
                    hierarchy = 1)

HIW = CouplingOrder(name = 'HIW',
                    expansion_order = 99,
                    hierarchy = 1)

