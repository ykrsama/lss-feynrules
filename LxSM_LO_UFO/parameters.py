# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.3.1 for Mac OS X ARM (64-bit) (July 24, 2021)
# Date: Thu 20 Jul 2023 19:54:05



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

cTheta = Parameter(name = 'cTheta',
                   nature = 'external',
                   type = 'real',
                   value = 0.01,
                   texname = '\\text{cTheta}',
                   lhablock = 'LxSMINPUTS',
                   lhacode = [ 1 ])

a2 = Parameter(name = 'a2',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{a2}',
               lhablock = 'LxSMINPUTS',
               lhacode = [ 2 ])

b3 = Parameter(name = 'b3',
               nature = 'external',
               type = 'real',
               value = 100,
               texname = '\\text{b3}',
               lhablock = 'LxSMINPUTS',
               lhacode = [ 3 ])

b4 = Parameter(name = 'b4',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{b4}',
               lhablock = 'LxSMINPUTS',
               lhacode = [ 4 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.000011663900000000002,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.118,
               texname = '\\text{aS}',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.42,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 174.3,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.0005110000000000001,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MM = Parameter(name = 'MM',
               nature = 'external',
               type = 'real',
               value = 0.10566,
               texname = '\\text{MM}',
               lhablock = 'MASS',
               lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.0025499999999999997,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.42,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

mh1 = Parameter(name = 'mh1',
                nature = 'external',
                type = 'real',
                value = 10,
                texname = '\\text{mh1}',
                lhablock = 'MASS',
                lhacode = [ 9999992 ])

mh2 = Parameter(name = 'mh2',
                nature = 'external',
                type = 'real',
                value = 125.1,
                texname = '\\text{mh2}',
                lhablock = 'MASS',
                lhacode = [ 9999993 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

sTheta = Parameter(name = 'sTheta',
                   nature = 'internal',
                   type = 'real',
                   value = 'cmath.sqrt(1 - cTheta**2)',
                   texname = '\\text{sTheta}')

Wh1 = Parameter(name = 'Wh1',
                nature = 'internal',
                type = 'real',
                value = '0.00407*cTheta**2',
                texname = '\\text{Wh1}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\text{aEW}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '0',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '0',
               texname = '\\text{ym}')

yu = Parameter(name = 'yu',
               nature = 'internal',
               type = 'real',
               value = '0',
               texname = '\\text{yu}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '1.001 - 0.0481*cmath.log(mh2/MT) + 0.0033*cmath.log(mh2/MT)**2 - 0.00023*cmath.log(mh2/MT)**3 + 8.35e-6*cmath.log(mh2/MT)**4',
               texname = '\\text{yt}')

yd = Parameter(name = 'yd',
               nature = 'internal',
               type = 'real',
               value = '0',
               texname = '\\text{yd}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '0',
               texname = '\\text{ys}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '0.02696 - 0.00314*cmath.log(mh2/MB) + 0.000286*cmath.log(mh2/MB)**2 - 0.0000182*cmath.log(mh2/MB)**3 + 6.5e-7*cmath.log(mh2/MB)**4',
               texname = '\\text{yb}')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

v = Parameter(name = 'v',
              nature = 'internal',
              type = 'real',
              value = '(2*MW*sw)/ee',
              texname = 'v')

b2 = Parameter(name = 'b2',
               nature = 'internal',
               type = 'real',
               value = 'cTheta**2*mh2**2 + mh1**2*sTheta**2 - (a2*v**2)/2.',
               texname = '\\text{b2}')

a1 = Parameter(name = 'a1',
               nature = 'internal',
               type = 'real',
               value = '(2*cTheta*(-mh1**2 + mh2**2)*sTheta)/v',
               texname = '\\text{a1}')

aah1 = Parameter(name = 'aah1',
                 nature = 'internal',
                 type = 'real',
                 value = '(47*aEW*(1 - (2*mh1**4)/(987.*MT**4) - (14*mh1**2)/(705.*MT**2) + (213*mh1**12)/(2.634632e7*MW**12) + (5*mh1**10)/(119756.*MW**10) + (41*mh1**8)/(180950.*MW**8) + (87*mh1**6)/(65800.*MW**6) + (57*mh1**4)/(6580.*MW**4) + (33*mh1**2)/(470.*MW**2)))/(18.*cmath.pi*v)',
                 texname = '\\text{aah1}')

ggh1 = Parameter(name = 'ggh1',
                 nature = 'internal',
                 type = 'real',
                 value = '-0.08333333333333333*(G**2*(0.0413312*2.731**((1.162015*mh1**2)/MT**2) + 0.9594200561691832*(mh1**2/MT**2)**0.0000728843 - 0.02626226043280332*(mh1**2/MT**2)**3.00962 - 0.00031001210717521843*(mh1**2/MT**2)**6.38565))/(cmath.pi**2*v)',
                 texname = '\\text{ggh1}')

ggh2 = Parameter(name = 'ggh2',
                 nature = 'internal',
                 type = 'real',
                 value = '-0.08333333333333333*(G**2*(-900.339 + 40.165/2.71**((0.1774765*mh2**2)/MT**2) + (5.85184 + (0.029328*mh2**2)/MT**2)**4 - (17.677 + (0.78934*mh2**2)/MT**2)**2 + 12.650003163832732*(mh2**2/MT**2)**0.940082))/(cmath.pi**2*v)',
                 texname = '\\text{ggh2}')

lmda = Parameter(name = 'lmda',
                 nature = 'internal',
                 type = 'real',
                 value = '(cTheta**2*mh1**2 + mh2**2*sTheta**2)/(2.*v**2)',
                 texname = '\\text{lmda}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*cmath.sqrt(2))/v',
               texname = '\\text{yc}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/v',
                 texname = '\\text{ytau}')

b1 = Parameter(name = 'b1',
               nature = 'internal',
               type = 'real',
               value = '-0.25*(a1*v**2)',
               texname = '\\text{b1}')

lmda211 = Parameter(name = 'lmda211',
                    nature = 'internal',
                    type = 'real',
                    value = '2*b3*cTheta*sTheta**2 + (a1*cTheta*(cTheta**2 - 2*sTheta**2))/2. - 6*cTheta**2*lmda*sTheta*v + a2*sTheta*(2*cTheta**2 - sTheta**2)*v',
                    texname = '\\text{lmda211}')

muSq = Parameter(name = 'muSq',
                 nature = 'internal',
                 type = 'real',
                 value = 'lmda*v**2',
                 texname = '\\text{muSq}')

Wh2 = Parameter(name = 'Wh2',
                nature = 'internal',
                type = 'real',
                value = '0.00407*sTheta**2 + (lmda211**2*cmath.sqrt(1 - (4*mh1**2)/mh2**2))/(32.*cmath.pi*mh2)',
                texname = '\\text{Wh2}')

