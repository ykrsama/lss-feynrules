(**********************************************************************)
(*                                                                    *)
(*                          LxSM Parameter File                        *)
(*           This file contains all the relevant parameters.          *)
(*                                                                    *)
(**********************************************************************)

M$Parameters = {

(***************************************************)
(*************** External parameters ***************)
(***************************************************)


    (*********************************)
    (*                               *)
    (*      LxSM External Params      *)
    (*                               *)
    (*********************************)


 cTheta == {
        ParameterType -> External,
        BlockName -> LxSMINPUTS,
        Value -> 0.01,
        ParameterName -> cTheta,
        Description -> "Cosine of the Higgs-Singlet mixing angle"},


 a2 == {
        ParameterType -> External,
        BlockName -> LxSMINPUTS,
        InteractionOrder -> {QED, 2},
        Value -> 1,
        ParameterName -> a2,
        Description -> "Higgs-Singlet quartic coupling"},


 b3 == {
        ParameterType -> External,
        BlockName -> LxSMINPUTS,
        InteractionOrder -> {QED, 1},
        Value -> 100,
        ParameterName -> b3,
        Description -> "Singlet cubic coupling"},


 b4 == {
        ParameterType -> External,
        BlockName -> LxSMINPUTS,
        InteractionOrder -> {QED, 2},
        Value -> 1,
        ParameterName -> b4,
        Description -> "Singlet quartic coupling"},



     (*********************************)
     (*                               *)
     (*     SM External Parameters    *)
     (*                               *)
     (*********************************)


  \[Alpha]EWM1== {
        ParameterType -> External,
        BlockName -> SMINPUTS,
        ParameterName -> aEWM1,
        InteractionOrder -> {QED, -2},
        Value -> 127.9,
        Description -> "Inverse of the electroweak coupling constant"},

  Gf == {
        ParameterType -> External,
        BlockName -> SMINPUTS,
        TeX -> Subscript[G, f],
        InteractionOrder -> {QED, 2},
        Value -> 1.16639 * 10^(-5),
        Description -> "Fermi constant"},

  \[Alpha]S == {
        ParameterType -> External,
        BlockName -> SMINPUTS,
        TeX -> Subscript[\[Alpha], s],
        ParameterName -> aS,
        InteractionOrder -> {QCD, 2},
        Value -> 0.118,
        Description -> "Strong coupling constant at the Z pole."},

  ymc == {
        ParameterType -> External,
        BlockName -> YUKAWA,
        Value -> 1.42,
        OrderBlock -> {4},
        Description -> "Charm Yukawa mass"},

 ymb == {
        ParameterType -> External,
        BlockName -> YUKAWA,
        Value -> 4.7,
        OrderBlock -> {5},
        Description -> "Bottom Yukawa mass"},

  ymt == {
        ParameterType -> External,
        BlockName -> YUKAWA,
        Value -> 174.3,
        OrderBlock -> {6},
        Description -> "Top Yukawa mass"},

  ymtau == {
        ParameterType -> External,
        BlockName -> YUKAWA,
        Value -> 1.777,
        OrderBlock -> {15},
        Description -> "Tau Yukawa mass"},

   cabi == {
        TeX -> Subscript[\[Theta], c],
        ParameterType -> External,
        BlockName -> CKMBLOCK,
        Value -> 0.227736,
        Description -> "Cabibbo angle"},


(***************************************************)
(*************** Internal parameters ***************)
(***************************************************)


    (*********************************)
    (*                               *)
    (*      LxSM Internal Params      *)
    (*                               *)
    (*********************************)


 sTheta == {
        ParameterType -> Internal,
        Value -> Sqrt[1-cTheta^2],
        ParameterName -> sTheta,
        Description -> "Sine of the Higgs-Singlet mixing angle"},


 lmda == {
        ParameterType -> Internal,
        InteractionOrder -> {QED, 2},
        Value -> (mh1^2 cTheta^2 + mh2^2 sTheta^2) / (2 v^2),
        ParameterName -> lmda,
        Description -> "Higgs doublet quartic coupling"},


 a1 == {
        ParameterType -> Internal,
        InteractionOrder -> {QED, 1},
        Value -> (mh2^2 - mh1^2) (2 sTheta cTheta) / v,
        ParameterName -> a1,
        Description -> "Higgs-Singlet cubic coupling"},


 b1 == {
        ParameterType -> Internal,
        Value -> - a1 v^2 / 4,
        ParameterName -> b1,
        Description -> "Singlet liner parameter"},


 b2 == {
        ParameterType -> Internal,
        Value -> -(a2 v^2 / 2) + mh2^2 cTheta^2 + mh1^2 sTheta^2,
        ParameterName -> b2,
        Description -> "Singlet quadratic parameter"},


 muSq == {
        ParameterType -> Internal,
        Value -> lmda v^2,
        ParameterName -> muSq,
        Description -> "Higgs doublet quadratic parameter"},


 lmda211 == {
        ParameterType -> Internal,
        Value -> 2 sTheta^2 cTheta b3 + a1 cTheta (cTheta^2 - 2 sTheta^2) / 2 + (2 cTheta^2 - sTheta^2) sTheta v a2 - 6 lmda sTheta cTheta^2 v,
        ParameterName -> lmda211,
        Description -> "Cubic coupling of h2h1h1"},


    (************************************************************)
    (*                                                          *)
    (*      LxSM Gluon-Gluon Fusion and Di-Photon Couplings      *)
    (*                                                          *)
    (************************************************************)


 ggh1 == {
        ParameterType -> Internal,
        InteractionOrder -> {HIG, 1},
        Value -> - gs^2 / (4 Pi 3 Pi v) ggH1[mh1^2 / (4 MT^2)],
        ParameterName -> ggh1,
        Description -> "5D Effective coupling of the unobserved eigenstate to gluons"},


 ggh2 == {
        ParameterType -> Internal,
        InteractionOrder -> {HIG, 1},
        Value -> - gs^2 / (4 Pi 3 Pi v) ggH2[mh2^2 / (4 MT^2)],
        ParameterName -> ggh2,
        Description -> "5D Effective coupling of the observed eigenstate to gluons"},


 aah1 == {
       	ParameterType -> Internal,
       	InteractionOrder -> {HIW, 1},
       	Value -> (aEW/(Pi v)) (47/18) yyH1[mh1^2/(2 MW)^2, mh1^2/(2 MT)^2],
       	ParameterName -> aah1,
       	Description -> "5D Effective coupling of the unobserved eigenstate to photons"},



    (**********************************************)
    (*                                            *)
    (*        LxSM Scalar Eigenstate Widths        *)
    (*                                            *)
    (**********************************************)


 Wh1 == {
        ParameterType -> Internal,
        Value -> cTheta^2 * 0.00407,
        ParameterName -> Wh1,
        Description -> "Width of the unobserved scalar eigenstate"},


 Wh2 == {
        ParameterType -> Internal,
        Value -> sTheta^2 * 0.00407 + lmda211^2 * Sqrt[1 - (4 mh1^2 / mh2^2)] / (32 Pi mh2),
        ParameterName -> Wh2,
        ComplexParameter -> False,
        Description -> "Width of the observed scalar eigenstate"},


     (*********************************)
     (*                               *)
     (*     SM Internal Parameters    *)
     (*                               *)
     (*********************************)


  \[Alpha]EW == {
        ParameterType -> Internal,
        Value -> 1/\[Alpha]EWM1,
        TeX -> Subscript[\[Alpha], EW],
        ParameterName -> aEW,
        InteractionOrder -> {QED, 2},
        Description -> "Electroweak coupling contant"},


  MW == {
        ParameterType -> Internal,
        Value -> Sqrt[MZ^2/2+Sqrt[MZ^4/4-Pi/Sqrt[2]*\[Alpha]EW/Gf*MZ^2]],
        TeX  -> Subscript[M, W],
        Description -> "W mass"},


  sw2 == {
        ParameterType -> Internal,
        Value -> 1-(MW/MZ)^2,
        Description -> "Squared Sin of the Weinberg angle"},


   ee == {
        TeX -> e,
        ParameterType -> Internal,
        Value -> Sqrt[4 Pi \[Alpha]EW],
        InteractionOrder -> {QED, 1},
        Description -> "Electric coupling constant"},


   cw == {
        TeX -> Subscript[c, w],
        ParameterType -> Internal,
        Value -> Sqrt[1 - sw2],
        Description -> "Cos of the Weinberg angle"},


   sw == {
        TeX -> Subscript[s, w],
        ParameterType -> Internal,
        Value -> Sqrt[sw2],
        Description -> "Sin of the Weinberg angle"},


   gw == {
        TeX -> Subscript[g, w],
        ParameterType -> Internal,
        Value -> ee / sw,
        InteractionOrder -> {QED, 1},
        Description -> "Weak coupling constant"},


   g1 == {
        TeX -> Subscript[g, 1],
        ParameterType -> Internal,
        Value -> ee / cw,
        InteractionOrder -> {QED, 1},
        Description -> "U(1)Y coupling constant"},


   gs == {
        TeX -> Subscript[g, s],
        ParameterType -> Internal,
        Value -> Sqrt[4 Pi \[Alpha]S],
        InteractionOrder -> {QCD, 1},
        ParameterName -> G,
        Description -> "Strong coupling constant"},


   v == {
        ParameterType -> Internal,
        Value -> 2*MW*sw/ee,
        InteractionOrder -> {QED, -1},
        Description -> "Higgs VEV"},


   yl == {
        TeX -> Superscript[y, l],
        Indices -> {Index[Generation]},
        AllowSummation -> True,
        ParameterType -> Internal,
        Value -> {yl[1] -> 0, yl[2] -> 0, yl[3] -> Sqrt[2] ymtau / v},
        ParameterName -> {yl[1] -> ye, yl[2] -> ym, yl[3] -> ytau},
        InteractionOrder -> {QED, 1},
        ComplexParameter -> False,
        Description -> "Lepton Yukawa coupling"},

(*   yu == {
        TeX -> Superscript[y, u],
        Indices -> {Index[Generation]},
        AllowSummation -> True,
        ParameterType -> Internal,
        Value -> {yu[1] -> 0, yu[2] -> Sqrt[2] ymc / v, yu[3] -> Sqrt[2] ymt / v},
        ParameterName -> {yu[1] -> yu, yu[2] -> yc, yu[3] -> yt},
        InteractionOrder -> {QED, 1},
        ComplexParameter -> False,
        Description -> "U-quark Yukawa coupling"}, *)

yu == {
        TeX -> Superscript[y, u],
        Indices -> {Index[Generation]},
        AllowSummation -> True,
        ParameterType -> Internal,
        Value -> {yu[1] -> 0, yu[2] -> Sqrt[2] ymc / v, yu[3] -> Yt[Log[mh2/MT]]},
        ParameterName -> {yu[1] -> yu, yu[2] -> yc, yu[3] -> yt},
        InteractionOrder -> {QED, 1},
        ComplexParameter -> False,
        Description -> "U-quark Yukawa coupling"},

   yd == {
        TeX -> Superscript[y, d],
        Indices -> {Index[Generation]},
        AllowSummation -> True,
        ParameterType -> Internal,
        Value -> {yd[1] -> 0, yd[2] -> 0, yd[3] -> Yb[Log[mh2/MB]]},
        ParameterName -> {yd[1] -> yd, yd[2] -> ys, yd[3] -> yb},
        InteractionOrder -> {QED, 1},
        ComplexParameter -> False,
        Description -> "D-quark Yukawa coupling"},

(* N. B. : only Cabibbo mixing! *)
  CKM == {
       Indices -> {Index[Generation], Index[Generation]},
       TensorClass -> CKM,
       Unitary -> True,
       Value -> {CKM[1,1] -> Cos[cabi],
                 CKM[1,2] -> Sin[cabi],
                 CKM[1,3] -> 0,
                 CKM[2,1] -> -Sin[cabi],
                 CKM[2,2] -> Cos[cabi],
                 CKM[2,3] -> 0,
                 CKM[3,1] -> 0,
                 CKM[3,2] -> 0,
                 CKM[3,3] -> 1},
       Description -> "CKM-Matrix"}


}
